<?php
/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 2/12/2019
 * Time: 7:39 PM
 */
$db = new PDO('mysql:host=localhost;dbname=tuto;charset=utf8', 'root', '');

//gets data from form
$name = $_GET['name'];
$surname = $_GET['surname'];


if (isset($name) && isset($surname)) {
    addUser($name, $surname, $db);
    echo '<div class="alert alert-success" role="alert">
    This is a success alert—check it out!
        </div>';
} else {
    echo '<div class="alert alert-danger" role="alert">
    Invalid AJAX call
        </div>';
}

function addUser($name, $surname, PDO $db)
{
    $req = $db->prepare('INSERT INTO user(name,surname) VALUES(?,?)');
    $req->execute(array($name, $surname));
}

echo "added : ".$name." ".$surname;