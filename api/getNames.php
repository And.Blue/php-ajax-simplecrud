<?php
/**
 * fetching user names, not OO for readability purposes
 * for json format :
header("Content-Type: application/json; charset=UTF-8");
$obj = json_decode($_GET["x"], false);
 */
$db = new PDO('mysql:host=localhost;dbname=tuto;charset=utf8', 'root', '');
$req = $db->query('SELECT name FROM user');
$data = $req->fetchAll(PDO::FETCH_NUM);
$req->closeCursor();

$names = array();

foreach ($data as $name){
    $names[] = $name[0]; //converts double to one single array to make decoding easier
}

echo json_encode($names);









