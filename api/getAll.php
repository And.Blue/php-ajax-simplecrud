<?php
/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 3/21/2019
 * Time: 5:04 PM
 */


$db = new PDO('mysql:host=localhost;dbname=tuto;charset=utf8', 'root', '');


$req = $db->query('SELECT * FROM user ORDER BY name DESC');
$data = $req->fetchAll();
$req->closeCursor();

echo json_encode($data);
