<?php
/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 3/21/2019
 * Time: 10:15 AM
 */

$db = new PDO('mysql:host=localhost;dbname=tuto;charset=utf8', 'root', '');

function addUser($name, $surname, PDO $db)
{
    $req = $db->prepare('INSERT INTO user(name,surname) VALUES(?,?)');
    $req->execute(array($name, $surname));
}


if(isset($_POST))
{
    addUser($_POST['name'],$_POST['surname'],$db);

    $_POST['status'] = 'success';
    echo json_encode($_POST);
}