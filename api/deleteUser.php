<?php
/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 2/13/2019
 * Time: 6:08 PM
 *
/**
 * Get users with name provided by Request
 */
$db = new PDO('mysql:host=localhost;dbname=tuto;charset=utf8', 'root', '');


$param = $_GET['id'];

$req = $db->prepare('DELETE FROM user WHERE id = ?');
$req->execute(array($param));

$data = $req->fetchAll();
$req->closeCursor();
