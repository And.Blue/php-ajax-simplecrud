<?php

/**
 * Get users with name provided by Request
 */
$db = new PDO('mysql:host=localhost;dbname=tuto;charset=utf8', 'root', '');


$param = $_GET['name'];
if ($param != '*') {
    $req = $db->prepare('SELECT * FROM user WHERE name = ?');
} else {
    $req = $db->prepare('SELECT * FROM user');
}
$req->execute(array($param));
$data = $req->fetchAll();
$req->closeCursor();


/**
 * Show the retrieved data from the server
 * Generates the table directly from the API, which isn't any good for a "usable" API,this really should be in the front
 * end !!!!!!!!!!!!!!!!
 */
if (count($data) < 1) {
    echo "No user found :'(";
} else {
    //echo(count($data) > 1 ? "Users" : "User");
    echo '<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Surname</th>
      <th scope="col">Created at</th>
      <th scope="col">Delete</th>
    </tr>
  </thead><tbody>';
    foreach ($data as $value) {
        echo "<tr><th scope='col'>" . $value['id'] . "</th><td>" . $value['name'] . "</td><td>" . $value['surname'] .  "</td><td>" . $value['create_at'] . "</td><td><button type=\"button\" class=\"btn btn-danger\" onclick='deleteUser(".$value['id'].")'>delete</button></td></tr>";
    }
    echo "</tbody>
</table>";
}


