<form>
    <input name="name" type="text" id="name3" placeholder="Name">
    <input name="surname" type="text" id="surname3" placeholder="Surname ">
    <button type="button" class="btn btn-primary mb-2 text-center" onclick="userAdd()">testPost</button>
</form>


<div id='result' class=""></div>

<script>
    var res = document.getElementById('result');
    var inputName = document.getElementById('name3');
    var inputSurname = document.getElementById('surname3');


    function userAdd() {
        if (res.innerText === '') {

            ajaxPost('api/postTest.php', 'name=' + inputName.value + '&surname=' + inputSurname.value, handleData);

        } else {
            res.innerHTML = '<span background="red">Nope, not filled</span>';
        }
    }

    function handleData(data) {
        if(data.status === 'success') {
            res.innerText = 'added : ' + data.name + ' ' + data.surname;
            inputName.value = '';
            inputSurname.value = '';
        } else {
            res.innerText = 'oups...something went wrong!';
        }


    }

    function ajaxPost(toUrl, param, callback) {
        var data; //response data
        var http = new XMLHttpRequest();
        var url = toUrl;
        var params = param;
        http.open('POST', url, true);

        http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        http.onreadystatechange = function () {//Call a function when the state changes.
            if (http.readyState == 4 && http.status == 200) {
                res.innerText = http.responseText;
                data = JSON.parse(http.response);
                callback(data);
            } else {
                data = {error: 'no user found'};
                callback(data);
            }
        };
        http.send(params);

    }


</script>