function showUser() {
    var xhttp;
    var str = document.getElementById("userReq").value;
    if (str === "") {
        document.getElementById("allUsers").innerHTML = "too bad, nobody is named nothing..";
        return;
    }
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("allUsers").innerHTML = this.responseText;
            document.getElementById("modalAllTitle").innerHTML = 'Search for ' + str;
        }
    };
    xhttp.open("GET", "api/getusers.php?name=" + str, true);
    xhttp.send();
}

function deleteUser(id) {
    var xhttp;
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (document.getElementById("modalAllTitle").innerHTML !== "All Users") {
                document.getElementById("allUsers").innerHTML = "deleted user";
                getNameArr(); //refreshes the names for suggestions
            } else showUserAll();

        }
    };
    xhttp.open("GET", "api/deleteuser.php?id=" + id, true);
    xhttp.send();
}

//this function could be totally rewritten into one that could replace showUser() as well
function showUserAll() {
    var xhttp;
    var str = '*';
    if (str === "") {
        document.getElementById("allUsers").innerHTML = "";
        return;
    }
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("allUsers").innerHTML = this.responseText;
            document.getElementById("modalAllTitle").innerHTML = "All Users";
        }
    };
    xhttp.open("GET", "api/getusers.php?name=" + str, true);
    xhttp.send();
}

function newUser() {
    var name = document.getElementById('name').value;
    var surname = document.getElementById('surname').value;
    var xhttp;


    if (isBlank(name) || isBlank(surname)) {
        document.getElementById("modalAlert").innerHTML =
            '<div class="alert alert-danger" role="alert">Name or surname isn\'t filled in!</div>';
    } else {
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {

                console.log(this.responseText);
                console.log("added " + name + " " + surname);
                getNameArr();
                document.getElementById("modalAlert").innerHTML =
                    '<div class="alert alert-success" role="alert">' + name + ' ' + surname + ' was added to database :)' +
                    'feel free to add another user!</div>';
            }
        };
        xhttp.open("GET", "api/insertuser.php?name=" + name + "&surname=" + surname, true);
        xhttp.send();
    }

}




//checks if string is blank
function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}

/**
 * resets modal when clicked on
 */
function resetModal() {
    document.getElementById("modalAlert").innerHTML = "";
    document.getElementById('name').value = "";
    document.getElementById('surname').value ="";
}
