getNameArr();
var names = [];

function showHint(str) {
    if (str.length == 0) {
        document.getElementById("txtHint").innerHTML = "";
    } else if (str == "date") {
        document.getElementById("txtHint").innerHTML = new Date().toLocaleTimeString();

    } else {
        var suggestion = (printArr(getHint(str)) ? printArr(getHint(str)) : "no suggestions :'(");
        document.getElementById("txtHint").innerHTML = 'Suggestions: '+suggestion;
    }

}

function getNameArr() {
    var res;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            console.log("GetNameArr() response received");
            res = JSON.parse(this.responseText);
            names = res;

        }
    };
    xmlhttp.open("GET", "api/getnames.php");
    xmlhttp.send();

}



function printArr(obj) {
    var x, txt="";
    for (x =0 ; x<obj.length; x++) {
        if(x === obj.length-1) {
            txt += obj[x] + ". ";
        } else {
            txt += obj[x] + " , ";
        }

    }
    return txt;
}


//compares the inputed letters to the names and gets an array of hints
function getHint(str) {
    var res = [];
    names.forEach(function (value) {

       if(value.toLowerCase().substring(0,str.length) == str.toLowerCase())
       {
           res.push(value.toLowerCase().trim()); //trims, lowers value
       }
    });
    //only return unique names
    res = res.filter(function (value,index,self) {
        return self.indexOf(value) === index;
    });
    return res;
}








